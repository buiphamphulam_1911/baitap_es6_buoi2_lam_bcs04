import {
  renderGlassesList,
  timKiemViTri,
  renderInfoGlasses,
  renderGlasses,
} from "./controller.js";
import { dataGlasses } from "../data/data.js";

renderGlassesList(dataGlasses);

function showThongTinLenForm(id) {
  let x = timKiemViTri(id, dataGlasses);

  document.getElementById("avatar").innerHTML = renderGlasses(dataGlasses[x]);
  document.getElementById("glassesInfo").innerHTML = renderInfoGlasses(
    dataGlasses[x]
  );
}



window.showThongTinLenForm = showThongTinLenForm;
